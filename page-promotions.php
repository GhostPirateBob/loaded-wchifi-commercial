<?php

get_header(); ?>


	<div class="center-site">
  <?php while ( have_posts() ) : the_post(); ?>

    <h1><?php the_title(); ?></h1>

    <div class="promos-container">
  <?php if( have_rows('promo_campaigns') ): ?>


  	<?php while( have_rows('promo_campaigns') ): the_row();

  		// vars
  		$image = get_sub_field('image');
  		$link = get_sub_field('link');
      $cat_link = get_sub_field('category_link');
      $cat_top = get_term ($cat_link);
  		?>

  		<div class="promo-campaign">



			<?php if( $link ): ?>
				<a href="<?php echo $link; ?>">
      <?php endif; ?>

      <?php if (get_sub_field('category_link')) : ?>
        <a href="<?php echo get_bloginfo('url').'/product-category/'.$cat_top->slug; ?>">
      <?php endif; ?>
      
          <div class="media-container">
  				  <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" class="media-item" />
          </div>

          <?php if (get_sub_field('category_link')) : ?>
        </a>
      <?php endif; ?>

			<?php if( $link ): ?>
				</a>
			<?php endif; ?>

  		</div>

  	<?php endwhile; ?>

  <?php endif; ?>

    </div>

  <?php endwhile; ?>

    </div>

	</div><!-- .center-site -->

<script>
// 	ga('send', 'event', 'Promotions', 'Store Specials', '<?php the_title() ?>');
</script>

<div class="cta-blocks">
  <div class="cta-item" id="cta-pricematch">
    <div class="overlay">
      <div class="media-container">
        <a href="<?php bloginfo('url'); ?>/pricematch"><?php echo file_get_contents(get_stylesheet_directory_uri() . "/assets/images/cta-pricematch.svg") ?></a>
      </div>
      <a href="<?php bloginfo('url'); ?>/pricematch" class="ci-button">Learn More ></a>
    </div>
  </div>
  <div class="cta-item" id="cta-store">
    <div class="overlay">
      <div class="media-container">
        <a href="<?php bloginfo('url'); ?>/store-locations/"><?php echo file_get_contents(get_stylesheet_directory_uri() . "/assets/images/nearest-store.svg") ?></a>
      </div>
      <a href="<?php bloginfo('url'); ?>/store-locations/" class="ci-button">Find a store ></a>
    </div>
  </div>
  <div class="cta-item" id="cta-clearance">
    <div class="overlay">
      <div class="media-container">
        <a href="<?php bloginfo('url'); ?>/clearance-centre/"><?php echo file_get_contents(get_stylesheet_directory_uri() . "/assets/images/cta-clearance.svg") ?></a>
      </div>
      <a href="<?php bloginfo('url'); ?>/clearance-centre/" class="ci-button">Grab a bargain ></a>
    </div>
  </div>
</div>


<?php
get_footer();

