<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *

 */
?>

        </div><!-- #main -->
    </div><!-- #page -->

  <footer id="footer-newsletter">
    <div class="container">
      <h2 class="title">Sign up to receive exclusive news and offers.</h2>
            <?php gravity_form('Newsletter Registration', false, false, false, '', true, 12); ?>
    </div>
  </footer>

    <footer id="" class="site-footer" role="contentinfo">
        <div class="container">

            <div class="column">
                <?php wp_nav_menu( array( 'theme_location' => 'footer-col-1') ); ?>
            </div>

            <div class="column">
                <?php wp_nav_menu( array( 'theme_location' => 'footer-col-2') ); ?>
            </div>

            <div class="column">
                <?php wp_nav_menu( array( 'theme_location' => 'footer-col-3') ); ?>
            </div>

            <div class="column">
                <?php wp_nav_menu( array( 'theme_location' => 'footer-col-4') ); ?>

                <h4 class="title">Payment options</h4>
                <div class="media-container payment">
                    <img srcset="<?php echo get_stylesheet_directory_uri();?>/assets/images/footer-mc.png 1x, <?php echo get_stylesheet_directory_uri();?>/assets/images/footer-mc@2x.png 2x" alt="MasterCard" class="media-item">
                </div>
                <div class="media-container payment">
                    <img srcset="<?php echo get_stylesheet_directory_uri();?>/assets/images/footer-visa.png 1x, <?php echo get_stylesheet_directory_uri();?>/assets/images/footer-visa@2x.png 2x" alt="Visa" class="media-item">
                </div>
                <div class="media-container payment">
                    <img srcset="<?php echo get_stylesheet_directory_uri();?>/assets/images/footer-paypal.png 1x, <?php echo get_stylesheet_directory_uri();?>/assets/images/footer-paypal@2x.png 2x" alt="Paypal" class="media-item">
                </div>

                <h4 class="title">Follow Us</h4>
                <a class="footer-social-link" href="https://www.facebook.com/westcoasthifi/" id="footer-facebook">
                    <?php echo file_get_contents(get_stylesheet_directory() . "/assets/images/facebook.svg") ?>
                </a>
                <a class="footer-social-link" href="https://www.instagram.com/westcoasthifi_/" id="footer-instagram">
                    <?php echo file_get_contents(get_stylesheet_directory() . "/assets/images/instagram.svg") ?>
                </a>
            </div>
        </div>
        <div class="container">
            <div class="copyright">
                &copy; <?php echo date('Y'); ?> West Coast Hifi
            </div>
            <div class="media-container monogram">
              <div class="mg-lrg">
                  <img srcset="<?php echo get_stylesheet_directory_uri();?>/assets/images/footer-monogram.png 1x, <?php echo get_stylesheet_directory_uri();?>/assets/images/footer-monogram@2x.png 2x" alt="" class="media-item">
              </div>
              <div class="mg-sml">
                  <img srcset="<?php echo get_stylesheet_directory_uri();?>/assets/images/footer-monogram-small.png 1x, <?php echo get_stylesheet_directory_uri();?>/assets/images/footer-monogram-small@2x.png 2x" alt="" class="media-item">
              </div>
            </div>
        </div>
    </footer><!-- #colophon -->

    <div id="search-modal">
      <div class="row">
        <div class="search-modal-content col-12">
        </div>
      </div>
    </div>

<script>
      jQuery(document).ready(function($){

        $('a[href^="tel"]').on('click',function(){
          var location = $(this).attr('name');
          ga('send', 'event', 'Click to call', 'click', location);
        });

        $('a[href^="mailto"]').on('click',function(){
          var title = document.title;
          var link = $(this).attr('href');
          ga('send', 'event', 'Email contact', link, title);
        });

        // $('.action-search a').on('click', function(e){
        //   e.preventDefault();
        //   $('.ac-searchbox').toggleClass('active');
        // });

        $('#menu-primary-navigation > .menu-item-has-children > a').on('click', function(e){
          if ($(window).width() < 768) {
            e.preventDefault();
            $(this).parents('.menu-item-has-children').toggleClass('active');
            $(this).siblings('.sub-menu').toggleClass('active');
          }
        });

      });

      jQuery('#toggle-search-dropdown').on('click', function(e){
          if (jQuery(window).width() < 1200) {
            if ( !jQuery('.searchwp-live-ajax').hasClass('init-hidden') ) {
              jQuery('.searchwp-live-ajax').addClass('init-hidden');
            }
            jQuery('.searchwp-live-ajax').clone(true).prependTo( ".search-modal-content" );
            jQuery('.search-modal-content .searchwp-live-ajax').removeClass('init-hidden').addClass('search-modal-clone');
            jQuery('input#s').attr('placeholder', 'I\'m, looking for…');
            jQuery('#search-modal').modal();
            console.log('yep');
          } else {
            jQuery('.searchwp-live-ajax').toggleClass('init-hidden');
          }
      });

      jQuery('#search-modal').on(jQuery.modal.BEFORE_CLOSE, function(event, modal) {
        jQuery('.search-modal-content .col-12' ).html('&nbsp;...');
      });
      
  </script>
  <script type="text/javascript">
    jQuery( window ).load(function() {
        var page_title = document.title;
        var share_url = window.location.href;
        var facebook_url = 'http://www.facebook.com/sharer.php?u='+share_url;
        var google_url = 'https://plus.google.com/share?url='+share_url;
        var twitter_url = 'http://twitter.com/share?text='+page_title+'&url='+share_url;
        var mail_url = 'mailto:?subject='+page_title+'&body=Check out this deal '+share_url;
        var fb_icon = '<?php echo get_stylesheet_directory_uri(); ?>/assets/images/fb.png';
        var links = '<a class="social-link facebook" href="'+facebook_url+'" target="_blank" rel="nofollow"></a>'+'<a class="social-link google" href="'+google_url+'" target="_blank" rel="nofollow"></a>'+'<a class="social-link twitter" href="'+twitter_url+'" target="_blank" rel="nofollow"></a>'+'<a class="social-link email" href="'+mail_url+'" target="_blank" rel="nofollow"></a>';
        jQuery('.sharing-button').after('<div id="sharing-holder">'+links+'</div>');
        jQuery( ".sharing-button" ).click(function() {
          if (jQuery( "#sharing-holder" ).hasClass('show-sharing')) {
            jQuery( "#sharing-holder" ).removeClass('show-sharing');
          }
          else {
            jQuery( "#sharing-holder" ).addClass('show-sharing'); 
          }
        });
    });
  </script>
<?php if (is_cart()) { ?>
  <script>
  function updateFreeShipping() {
    var currency = jQuery('.cart-subtotal .woocommerce-Price-amount').text();
    var subtotalAmount = Number(currency.replace(/[^0-9.-]+/g,""));
    if ( subtotalAmount >= 100 && jQuery('#shipping_method_0_free_shipping2').prop('checked') !== true) {
      console.log(subtotalAmount);
      jQuery('#shipping_method_0_free_shipping2').prop('checked', true);
      jQuery('input#shipping_method_0_flat_rate1').parent().css('opacity', '0');

    } else if ( subtotalAmount < 100 && jQuery('#shipping_method_0_free_shipping2').prop('checked') === true) {
      jQuery('#shipping_method_0_flat_rate1').prop('checked', true);
      jQuery('input#shipping_method_0_flat_rate1').parent().css('opacity', '1');
    }
  }
  jQuery( document ).ready(function () {
    console.log('document.ready');
    updateFreeShipping();
  });
  </script>
<?php } ?>
<?php if (is_product()) { ?>
  <script>
jQuery(document).ready(function () {
  var singlePrice = "";
  var varPrice = "";
  if (jQuery('#zip-tagline').length > 0) {
    console.log('#zip-tagline price selected: ' + jQuery('meta[itemprop="price"]').attr('content'));
    singlePrice = Number(jQuery('meta[itemprop="price"]').attr('content') / 6);
    singlePrice = singlePrice.toLocaleString('en-AU', { style: 'currency', currency: 'AUD' });
    if (jQuery('#zipIncremental').length === 0) {
      jQuery('.entry-summary .price').after('<p id="zipIncremental" class="mb-0"><strong>or 6 &times; payments of ' + singlePrice + ' with zip</strong></p>');
    }
    if (jQuery('#zipIncremental').length > 0) {
      jQuery('#zipIncremental').html('<strong>or 6 &times; payments of ' + singlePrice + ' with zip</strong>');
    }

  }
});
  </script>
<?php } ?>
<?php wp_footer(); ?>
<?php if ( is_page_template('page-commercial-landing.php') ) { ?>
<script>

</script>
<?php } ?>
</body>
</html>