<?php
/**
 * The Header for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

global $woocommerce;

?>
<!DOCTYPE html>
<html <?php
      schema_org_markup();?> <?php language_attributes();?>>

<head>
  <meta name="google-site-verification" content="X79vHMHAaPyfqGMLioT1k7iwwzcc5HxNwwtnafK0IYU" />
  <meta charset="<?php bloginfo('charset');?>">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, viewport-fit=cover">
  <title>
    <?php wp_title('|', true, 'right');?>
  </title>
  <script src="//use.typekit.net/ckt4kpt.js"></script>
  <script>
    try {
      Typekit.load();
    }
    catch ( e ) {}

  </script>
  <script>
    // Picture element HTML5 shiv
    document.createElement( "picture" );

  </script>
  <script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/vendor/picturefill.min.js" async></script>
  <link href="https://fonts.googleapis.com/css?family=Ubuntu:300,400,700" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
  <?php if ( is_user_logged_in() == true ) {
        $wchifiAdminStyle = file_get_contents('/home/westcoasthifi/www/wp-content/themes/wchifi-2017/assets/stylesheets/css/wchifi-admin.css');
        if ( strlen($wchifiAdminStyle) > 10 ) {
            echo '<style type="text/css">'.$wchifiAdminStyle.'</style>';
        }
        $wchifiAdminBar = file_get_contents('/home/westcoasthifi/www/wp-content/themes/wchifi-2017/assets/stylesheets/css/wchifi-adminbar.css');
        if ( strlen($wchifiAdminBar) > 10 ) {
            echo '<style  type="text/css">'.$wchifiAdminBar.'</style>';
        }
    }
?>
  <?php wp_head();?>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui-touch-punch/0.2.3/jquery.ui.touch-punch.min.js"></script>
  <?php if (is_page_template('template-clearance.php')): ?>
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/js/vendor/tablesaw/tablesaw.css">
  <script src="<?php echo get_template_directory_uri(); ?>/js/vendor/tablesaw/tablesaw.js"></script>
  <script src="<?php echo get_template_directory_uri(); ?>/js/vendor/tablesaw/tablesaw-init.js"></script>
  <?php endif;?>
  <!-- Google Analytics -->
  <script>
    window.ga = window.ga || function () {
      ( ga.q = ga.q || [] ).push( arguments )
    };
    ga.l = +new Date;
    ga( 'create', 'UA-62444323-1', 'auto' );
    ga( 'send', 'pageview' );

  </script>
  <script async src='https://www.google-analytics.com/analytics.js'></script>
  <!-- End Google Analytics -->
  <!-- Google Tag Manager -->
  <script>
    ( function ( w, d, s, l, i ) {
      w[ l ] = w[ l ] || [];
      w[ l ].push( {
        'gtm.start': new Date().getTime(),
        event: 'gtm.js'
      } );
      var f = d.getElementsByTagName( s )[ 0 ],
        j = d.createElement( s ),
        dl = l != 'dataLayer' ? '&l=' + l : '';
      j.async = true;
      j.src =
        'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
      f.parentNode.insertBefore( j, f );
    } )( window, document, 'script', 'dataLayer', 'GTM-WFZRLV7' );

  </script>
  <!-- End Google Tag Manager -->
  <link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicons/favicon.ico" type="image/x-icon" />
  <link rel="apple-touch-icon" sizes="57x57" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicons/apple-touch-icon-57x57.png">
  <link rel="apple-touch-icon" sizes="60x60" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicons/apple-touch-icon-60x60.png">
  <link rel="apple-touch-icon" sizes="72x72" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicons/apple-touch-icon-72x72.png">
  <link rel="apple-touch-icon" sizes="76x76" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicons/apple-touch-icon-76x76.png">
  <link rel="apple-touch-icon" sizes="114x114" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicons/apple-touch-icon-114x114.png">
  <link rel="apple-touch-icon" sizes="120x120" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicons/apple-touch-icon-120x120.png">
  <link rel="apple-touch-icon" sizes="144x144" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicons/apple-touch-icon-144x144.png">
  <link rel="apple-touch-icon" sizes="152x152" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicons/apple-touch-icon-152x152.png">
  <link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicons/apple-touch-icon-180x180.png">
  <link rel="icon" type="image/png" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicons/favicon-16x16.png" sizes="16x16">
  <link rel="icon" type="image/png" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicons/favicon-32x32.png" sizes="32x32">
  <link rel="icon" type="image/png" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicons/favicon-96x96.png" sizes="96x96">
  <link rel="icon" type="image/png" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicons/android-chrome-192x192.png" sizes="192x192">
  <meta name="msapplication-square70x70logo" content="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicons/smalltile.png" />
  <meta name="msapplication-square150x150logo" content="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicons/mediumtile.png" />
  <meta name="msapplication-wide310x150logo" content="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicons/widetile.png" />
  <meta name="msapplication-square310x310logo" content="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicons/largetile.png" />
  <!-- Facebook Pixel Code -->
  <script>
    ! function ( f, b, e, v, n, t, s ) {
      if ( f.fbq ) return;
      n = f.fbq = function () {
        n.callMethod ?
          n.callMethod.apply( n, arguments ) : n.queue.push( arguments )
      };
      if ( !f._fbq ) f._fbq = n;
      n.push = n;
      n.loaded = !0;
      n.version = '2.0';
      n.queue = [];
      t = b.createElement( e );
      t.async = !0;
      t.src = v;
      s = b.getElementsByTagName( e )[ 0 ];
      s.parentNode.insertBefore( t, s )
    }( window,
      document, 'script', 'https://connect.facebook.net/en_US/fbevents.js' );
    fbq( 'init', '402216683473230' ); // Insert your pixel ID here.
    fbq( 'track', 'PageView' );

  </script>
  <?php if (is_page('checkout')): ?>
  <script>
    fbq( 'track', 'InitiateCheckout' );

  </script>
  <?php endif; ?>
  <noscript><img height="1" width="1" style="display: none;" src="https://www.facebook.com/tr?id=402216683473230&ev=PageView&noscript=1" /></noscript>
  <style type="text/css">
.woocommerce #content { padding-left: 15px; padding-right: 15px; }  ul.tabs.wc-tabs { margin-left: -15px !important; margin-right: -23px !important; padding: 0 !important; margin-bottom: 30px !important; }  .fs-wrapper { display: block; padding-left: 15px; margin-top: -10px; float: right; max-width: 100%; width: calc(100% - 174px); }  .fs-heading { font-size: 0.8rem; text-transform: uppercase; font-weight: 700; line-height: 0.8rem; display: block; margin-bottom: 4px; }  .woocommerce-cart #coupon_code { width: 120px !important; max-width: 100%; margin-right: 8px; }  .woocommerce-cart .coupon { display: flex; align-items: center; }  .woocommerce ul#shipping_method li, .woocommerce-page ul#shipping_method li { text-indent: inherit; padding: 4px 20px 4px 4px; margin: 0; list-style: none; display: flex; align-items: baseline; justify-content: stretch; width: 100%; line-height: 1.2; }  .shipping_method>li { transition: opacity 0.15s linear; }  .woocommerce ul#shipping_method .amount, .woocommerce-page ul#shipping_method .amount { margin-bottom: 0; }  .woocommerce ul#shipping_method li input, .woocommerce-page ul#shipping_method li input { position: relative; left: -5px; }  .fs-subheading { font-size: 0.84rem; font-weight: 400; line-height: 0.9rem; display: block; }  .fs-wrapper .fa { color: #42547c; transform: rotateY(180deg); font-size: 2.5rem; }  .fs-left-column { width: 2.5rem; display: inline-block; }  .fs-right-column { width: calc(100% - 2.5rem - 12px); display: inline-block; padding-left: 10px; }  @media only screen and (min-width: 481px) and (max-width: 770px) { .fs-wrapper { width: 100%; float: left; margin-top: 20px; padding-left: 0; } }  @media only screen and (max-width: 393px) { .fs-wrapper { width: 100%; float: left; margin-top: 20px; padding-left: 0; } }  .woocommerce #secondary .widget h1, .woocommerce-page #secondary .widget h1 { border-radius: 5px; background: #45547a; color: #fff; font-weight: 700; border-bottom: 3px solid #fccc1d; padding: 4px 10px; }  #primary-sidebar { margin-top: 26px; }  #woocommerce_price_filter-2 .ui-slider { margin: 22px 6px 25px !important; }  h1.woocommerce-products-header__title.page-title { margin-top: 25px !important; color: #45547a !important; font-weight: 700 !important; }  textarea#input_2_4 { max-width: 497px; }  select#input_2_5 { border-color: #e5e5e5; }  span.edit-link { display: none !important; }  .woocommerce .widget_price_filter .ui-slider .ui-slider-handle, .woocommerce-page .widget_price_filter .ui-slider .ui-slider-handle { background: #45547a; }  .ui-slider-range.ui-widget-header.ui-corner-all { background: #fccc1d !important; }  span.woocommerce-Price-amount.amount { display: inline-block; margin-bottom: 10px; }  .home .site-footer>.container:last-of-type { padding-bottom: 62px !important; }  html { margin-top: 0 !important; }  .superbar { position: fixed !important; bottom: 0px !important; }  .home .sp-slide { height: auto !important; }  #wchifi-homepage-current>ul>li[data-color="blue"] { background: #6c7894; }  #wchifi-homepage-current>ul>li[data-color="light"] { background: #fff; }  .page-id-16472 #main { padding: 0 !important; margin: 0 !important; }  .page-id-16472 #main-content { padding: 0 !important; margin: 0 !important; }  .footer-social-link, #footer-instagram, #footer-facebook { display: inline-block; width: 36px; height: 36px; }  #footer-instagram svg { -webkit-transition: fill 0.2s linear; transition: fill 0.2s linear; fill: #FFFFFF; }  #footer-instagram:hover svg { -webkit-transition: fill 0.2s linear; transition: fill 0.2s linear; fill: #FCCC1D; }  #footer-facebook { margin-right: 10px; }  @media only screen and (max-width: 768px) { .superbar { display: none !important; } }  .search-modal-content.col-12 { padding: 0 !important; }  #search-modal { opacity: 0.95; }  li.menu-icon-li .js-reveal:last-of-type { display: none; }  .home #main .tp-bullets { top: calc(100% - 32px) !important; opacity: .75; }  .woo_discount_rules_table .wdr_tr_head, .wdr_tr_body .wdr_td_body_range, .wdr_tr_body .wdr_td_body_discount { display: none; }  .price { margin-bottom: 10px; }  .wdr_td_body_title { font-weight: 600; }  span.woocommerce-Price-amount.amount { margin-bottom: 0; }  .mb-0 { margin-bottom: 5px; }  .testfreaks-badge { margin-bottom: 10px; }  #zipIncremental strong { display: inline-block; width: 100%; } .px-0 { padding-left: 0 !important; padding-right: 0 !important; }
  </style>
  <script async src="https://js.testfreaks.com/onpage/westcoasthifi.com.au/head.js"></script>
</head>

<body <?php
      body_class();?>> <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WFZRLV7" height="0" width="0"
      style="display: none; visibility:hidden;"></iframe></noscript>
  <div id="page" class="hfeed site">
    <header id="masthead">
      <div class="actions">
        <div class="container">
          <div class="row justify-content-between">
            <div class="store-selector col-auto">
              <!--
          <select id="select-store-field">
            <option>Please select</option>
            <option>Osborne Park</option>
            <option>O'Connor</option>
          </select>
-->
              <?php echo loaded_select_store() ?>
            </div>
            <ul class="nav-menu col-auto">
              <li class="action-search action-search-li">
                <button id="toggle-search-dropdown" type="button" class="btn-top-link">
                  <div class="icon">
                    <?php echo file_get_contents(get_stylesheet_directory() . "/assets/images/icon-search.svg") ?>
                  </div>
                  <span class="search-dropdown-heading">Product Search</span>
                </button>
                <div class="searchwp-live-ajax init-hidden">
                  <form action="<?php bloginfo('url');?>/" method="get">
                    <p>
                      <input class="searchwp-input-text" type="text" name="s" id="s" value="" data-swplive="true" />
                      <input id="post_type" type="hidden" name="post_type" value="product" />
                      <button type="submit" id="searchwp-submit-button" type="submit">
                        <span id="searchwp-submit-icon"
                          style="background-image: url('data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCA1MTIgNTEyIj48cGF0aCBmaWxsPSIjZmZmZmZmIiBkPSJNMjEwLjcgMTQ3LjZjNy41LTcuNSAxOS44LTcuNSAyNy4zIDBsOTUuNCA5NS43YzcuMyA3LjMgNy41IDE5LjEuNiAyNi42bC05NCA5NC4zYy0zLjggMy44LTguNyA1LjctMTMuNyA1LjctNC45IDAtOS45LTEuOS0xMy42LTUuNi03LjUtNy41LTcuNi0xOS43IDAtMjcuM2w3OS45LTgxLjEtODEuOS04MS4xYy03LjYtNy40LTcuNi0xOS42IDAtMjcuMnoiLz48cGF0aCAgZmlsbD0iI2ZmZmZmZiIgZD0iTTQ4IDI1NmMwIDExNC45IDkzLjEgMjA4IDIwOCAyMDhzMjA4LTkzLjEgMjA4LTIwOFMzNzAuOSA0OCAyNTYgNDggNDggMTQxLjEgNDggMjU2em0zMiAwYzAtNDcgMTguMy05MS4yIDUxLjYtMTI0LjRDMTY0LjggOTguMyAyMDkgODAgMjU2IDgwczkxLjIgMTguMyAxMjQuNCA1MS42QzQxMy43IDE2NC44IDQzMiAyMDkgNDMyIDI1NnMtMTguMyA5MS4yLTUxLjYgMTI0LjRDMzQ3LjIgNDEzLjcgMzAzIDQzMiAyNTYgNDMycy05MS4yLTE4LjMtMTI0LjQtNTEuNkM5OC4zIDM0Ny4yIDgwIDMwMyA4MCAyNTZ6Ii8+PC9zdmc+');"></span>
                      </button>
                    </p>
                  </form>
                </div>
              </li>
              <li class="menu-icon-li">
                <a href="#" id="reveal-recent-products" class="js-reveal">
                  <div class="icon">
                    <?php echo file_get_contents(get_stylesheet_directory() . "/assets/images/icon-clock.svg") ?>
                  </div>
                </a>
                <?php if (dynamic_sidebar('masthead')): else:endif;?>
              </li>
              <li class="action-cart-li" id="actions-cart">
                <a href="<?php bloginfo('url');?>/cart">
                  <div class="icon">
                    <?php
$cartItems = $woocommerce->cart->cart_contents_count;
if ($cartItems > 0) {
    ?>
                    <div class="badge">
                      <?php
echo sprintf(_n('%d', '%d', $woocommerce->cart->cart_contents_count, 'woothemes'), $woocommerce->cart->cart_contents_count);
    ?>
                    </div>
                    <?php }?>
                    <?php echo file_get_contents(get_stylesheet_directory() . "/assets/images/icon-cart.svg") ?>
                  </div> Cart
                </a>
              </li>
              <li class="menu-toggle-li">
                <a href="#" id="menu-toggle"><i class="burger-icon"></i> <span>Menu</span></a>
              </li>
            </ul>
          </div>
        </div>
      </div>
      <div class="mh-core">
        <div class="container">
          <div class="logo media-container">
          <?php if ( get_page_template_slug() === 'page-commercial-landing.php' ) { ?>
            <a href="<?php bloginfo('url'); ?>">
              <img
                srcset="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/logo-commercial.png 1x, <?php echo get_stylesheet_directory_uri(); ?>/assets/images/logo-commerical@2x.png 2x"
                alt="<?php echo get_bloginfo('title'); ?>">
            </a>
          <?php } else { ?>
            <a href="<?php bloginfo('url'); ?>">
              <img
                srcset="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/logo.png 1x, <?php echo get_stylesheet_directory_uri(); ?>/assets/images/logo@2x.png 2x"
                alt="<?php echo get_bloginfo('title'); ?>">
            </a>
          <?php } ?>
          </div>
          <div class="nav-container row">
            <?php wp_nav_menu(array('theme_location' => 'masthead', 'menu_class' => 'nav-menu'));?>
          </div>
        </div>
      </div>
    </header>
    <?php if ( is_singular('product') && (has_term( 'home-theatre-packs', 'product_cat'))  || 
      is_singular('product') && (has_term( 'lifestyle-packages', 'product_cat')) ) { ?>
    <div class="htp-main-wrapper">
      <?php } else { ?>
      <div id="main" class="site-main">
        <?php  } ?>
