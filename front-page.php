<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other 'pages' on your WordPress site will use a different template.

 */

get_header(); ?>

<?php /* 
<div class="banner-container">
  <?php echo do_shortcode('[sliderpro id="1"]'); ?>
</div>


<div class="revoslider-container">
  <?php putRevSlider( 'wchifi-homepage-current' ); ?>
</div>

*/ ?>

  <?php if ( get_field('revolution_slider') ) { ?>
    <?php $revoSlider = get_field('revolution_slider'); ?>
    <?php do_shortcode('[rev_slider alias="'. $revoSlider .'"]'); ?>
    <?php }?> 

  

<div class="product-carousel">
  <div class="container">

  <?php if( have_rows('carousel_item') ): ?>

    <div class="product-categories">

    <?php while( have_rows('carousel_item') ): the_row();

      // vars
      $image = get_sub_field('carousel_item_image');
      $title = get_sub_field('carousel_item_text');

      if (get_sub_field('carousel_item_ext_link')) {
        $term = get_sub_field('carousel_item_ext_link');
      } else {
        $term = get_term_link(get_sub_field('carousel_item_link'), 'product_cat');
      }

    ?>

      <div class="pc-item media-container">
        <a href="<?php echo $term; ?>">
          <img src="<?php echo $image; ?>" class="media-item" alt="" />
          <div><?php echo $title; ?></div>
        </a>
      </div>

    <?php endwhile; ?>

    </div>

  <?php endif; ?>
  </div>
</div>

<div class="home-content">
  <div class="container">

    <div id="the_content">
      <?php the_content(); ?>
    </div>

    <div class="feature-blocks">
      <div class="fb-item fb-packages">
        <div class="fbi-copy">
          <h2>Home Theatre Packages</h2>
          <p>Experience the larger than life movie experience at home.</p>
          <a href="<?php bloginfo('url'); ?>/home-theatre-packages/" class="btn">View our packages</a>
        </div>
      </div>
      <div class="fb-item fb-catalogue">
        <div class="fbi-copy">
          <h2>Latest Catalogue</h2>
          <p>Page after page of incredible savings on all your audio and video needs.</p>
          <a href="<?php bloginfo('url'); ?>/promos/catalogue/ " class="btn">Take a Look</a>
        </div>
      </div>

      <div class="fb-item fb-brands">
        <div class="fbi-copy">
          <h2>Our Brands</h2>
          <p>Perth’s biggest range of leading brands in home entertainment.</p>
          <div class="fb-brands_list">
            <div class="brand media-container">
              <a href="<?php bloginfo('url'); ?>/brand/bose">
                <img srcset="<?php echo get_stylesheet_directory_uri();?>/assets/images/logo-bose.png 1x, <?php echo get_stylesheet_directory_uri();?>/assets/images/logo-bose@2x.png 2x" alt="Bose" class="media-item">
              </a>
            </div>
            <div class="brand media-container">
              <a href="<?php bloginfo('url'); ?>/brand/bowers-and-wilkins/">
                <img srcset="<?php echo get_stylesheet_directory_uri();?>/assets/images/logo-bw.png 1x, <?php echo get_stylesheet_directory_uri();?>/assets/images/logo-bw@2x.png 2x" alt="Bowers &amp; Wilkins" class="media-item">
              </a>
            </div>
            <div class="brand media-container">
              <a href="<?php bloginfo('url'); ?>/brand/yamaha/">
                <img srcset="<?php echo get_stylesheet_directory_uri();?>/assets/images/logo-yamaha.png 1x, <?php echo get_stylesheet_directory_uri();?>/assets/images/logo-yamaha@2x.png 2x" alt="yamaha" class="media-item">
              </a>
            </div>
            <div class="brand media-container">
              <a href="<?php bloginfo('url'); ?>/brand/sonos/">
                <img srcset="<?php echo get_stylesheet_directory_uri();?>/assets/images/logo-sonos.png 1x, <?php echo get_stylesheet_directory_uri();?>/assets/images/logo-sonos@2x.png 2x" alt="sonos" class="media-item">
              </a>
            </div>
            <div class="brand media-container">
              <a href="<?php bloginfo('url'); ?>/brand/epson/">
                <img srcset="<?php echo get_stylesheet_directory_uri();?>/assets/images/logo-epson.png 1x, <?php echo get_stylesheet_directory_uri();?>/assets/images/logo-epson@2x.png 2x" alt="Epson" class="media-item">
              </a>
            </div>
            <div class="brand media-container">
              <a href="<?php bloginfo('url'); ?>/brand/denon/">
                <img srcset="<?php echo get_stylesheet_directory_uri();?>/assets/images/logo-denon.png 1x, <?php echo get_stylesheet_directory_uri();?>/assets/images/logo-denon@2x.png 2x" alt="Denon" class="media-item">
              </a>
            </div>
          </div>
          <a href="<?php bloginfo('url'); ?>/brands" class="btn">More brands</a>
        </div>
      </div>

      <div class="fb-item fb-auto">
        <div class="fbi-copy">
          <h2>Home Automation</h2>
          <p>Control your home at the touch of a button. The possibilities are infinite.</p>
          <a href="<?php bloginfo('url'); ?>/smart-home/" class="btn btn-white">Explore</a>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="cta-blocks">
  <div class="cta-item" id="cta-pricematch">
    <div class="overlay">
      <div class="media-container">
        <a href="<?php bloginfo('url'); ?>/pricematch"><?php echo file_get_contents(get_stylesheet_directory() . "/assets/images/cta-pricematch.svg") ?></a>
      </div>
      <a href="<?php bloginfo('url'); ?>/pricematch" class="ci-button">Learn More ></a>
    </div>
  </div>
  <div class="cta-item" id="cta-interestfree">
    <div class="overlay">
      <div class="media-container">
        <a href="<?php bloginfo('url'); ?>/interest-free-finance/"><?php echo file_get_contents(get_stylesheet_directory() . "/assets/images/cta-interestfree.svg") ?></a>
      </div>
      <a href="<?php bloginfo('url'); ?>/interest-free-finance/" class="ci-button">Find out more ></a>
    </div>
  </div>
  <div class="cta-item" id="cta-clearance">
    <div class="overlay">
      <div class="media-container">
        <a href="<?php bloginfo('url'); ?>/clearance-centre/"><?php echo file_get_contents(get_stylesheet_directory() . "/assets/images/cta-clearance.svg") ?></a>
      </div>
      <a href="<?php bloginfo('url'); ?>/clearance-centre/" class="ci-button">Grab a bargain ></a>
    </div>
  </div>
</div>


<script type="text/javascript">
  jQuery(document).ready(function($){

    checkProductCarousel();

    var resizeTimer;

    $(window).on('resize', function(e) {

      clearTimeout(resizeTimer);
      resizeTimer = setTimeout(function() {

        checkProductCarousel();

      }, 250);

    });

    function checkProductCarousel(){
      if ($(".product-carousel").css("overflow") == "hidden" ){
        $( ".product-categories" ).draggable({
          axis: "x",
          drag: function(event, ui) {
              var leftPosition = ui.position.left;
    //           console.log(leftPosition);

              if (leftPosition > 200) {
                ui.position.left = 200;
              } else if (leftPosition < -600) {
                ui.position.left = -600;
              }
          }
        });
      }
    }

  });
</script>

<?php get_footer(); ?>
