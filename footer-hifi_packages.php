<div id="htp_enquiry-form-wrapper">
  <div class="container">
    <?php gravity_form( 'Speak to an expert', $display_title = true, $display_description = true, $display_inactive = false, $field_values = null, $ajax = true, $tabindex, $echo = true ); ?>
  </div>
</div>
<script src="<?php echo get_stylesheet_directory_uri();?>/assets/vendor/jquery.customSelect.min.js" charset="utf-8"></script>
<script>
  jQuery(document).ready(function(){
    jQuery(document).bind('gform_post_render', function() {
      jQuery('#input_3_5').customSelect();
      jQuery('#input_6_5').customSelect();
    });
  });
</script>
