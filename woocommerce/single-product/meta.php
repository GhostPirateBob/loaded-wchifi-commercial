<?php
/**
 * Single Product Meta
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/meta.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

global $product;
?>
<div class="product_meta">

	<?php do_action( 'woocommerce_product_meta_start' ); ?>

	<?php if ( wc_product_sku_enabled() && ( $product->get_sku() || $product->is_type( 'variable' ) ) ) : ?>

		<span class="sku_wrapper"><?php esc_html_e( 'SKU:', 'woocommerce' ); ?> <span class="sku"><?php echo ( $sku = $product->get_sku() ) ? $sku : esc_html__( 'N/A', 'woocommerce' ); ?></span></span>

	<?php endif; ?>

	<?php echo wc_get_product_category_list( $product->get_id(), ', ', '<span class="posted_in">' . _n( 'Category:', 'Categories:', count( $product->get_category_ids() ), 'woocommerce' ) . ' ', '</span>' ); ?>

	<?php echo wc_get_product_tag_list( $product->get_id(), ', ', '<span class="tagged_as">' . _n( 'Tag:', 'Tags:', count( $product->get_tag_ids() ), 'woocommerce' ) . ' ', '</span>' ); ?>

	<?php //do_action( 'woocommerce_product_meta_end' ); ?>


<?php if (get_post_meta( $product->id, 'instore', true) === 'yes'): ?>
<h4 class="mt-3">THIS PRODUCT IS NOT AVAILABLE ONLINE.<br>PURCHASE AT STORE ONLY.</h4>
<a id="instoreModalbtn" class="button" href="#instoreModal" rel="modal:open">Contact Us to Buy</a>
<div id="instoreModal" class="modal">
<h1><?php echo $product->name;?></h1>
<?php gravity_form( 11, false, true, false, '', true, 100, true);?>
</div>
<?php endif; ?>
</div>
          
<?php $exclusive_stores = loaded_get_exclusive_stores($product->id); if (count($exclusive_stores->slugs) ) : ?>
<p id="exclusive-store-msg" class="<?php the_field('store') ?>" style="color: #D3261F"><strong>YOU MUST SELECT WESTCOAST HI-FI <?php echo strtoupper($exclusive_stores->nice_or) ?> STORE TO PURCHASE THIS PRODUCT.</strong></p>
<?php endif ?>
