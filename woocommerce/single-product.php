<?php
/**
 * The Template for displaying all single products
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @author         WooThemes
 * @package     WooCommerce/Templates
 * @version     1.6.4
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

get_header('shop');
$baseUrl = site_url();
$product = new WC_Product( get_the_ID() );

$globalData = array(
  'baseUrl' => $baseUrl
);
?>


<?php if ( is_singular('product') && (has_term( 'home-theatre-packs', 'product_cat')) || is_singular('product') && (has_term( 'lifestyle-packages', 'product_cat')) ) : ?>
<?php
if ( has_term( 'home-theatre-packs', 'product_cat') ) {
  $faqPageID = 13834;
} elseif ( has_term( 'lifestyle-packages', 'product_cat') ) {
  $faqPageID = 21695;
}
?>
<?php if ( has_term( 'lifestyle-packages', 'product_cat') ) : ?>
<script>
jQuery(document).ready(function(){
  jQuery('.BannerInner__actionButton--3p5gM').attr('href', '<?php echo get_the_permalink(21695); ?>');
});
</script>
<?php endif; ?>
<div id="app-globals" data-appdata='<?php echo json_encode($globalData, JSON_HEX_APOS); ?>'></div>
<?php
  $appcss = '/assets/components/css/htp_inner.css';
  $appmodDate = '?v=' . (filemtime(get_stylesheet_directory() . $appcss));
?>
<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri() . $appcss . $appmodDate ?>" type="text/css" charset="utf-8">

<?php
  $faqItems = array();

  if( have_rows('faq_posts', $faqPageID) ):
    while ( have_rows('faq_posts', $faqPageID) ) : the_row();
      $post = get_sub_field('link');

      if ($post) :
        setup_postdata( $post );
        $postLink = get_the_permalink();
        $postId = get_the_ID();
        $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), array(250,166) );
        $imageSrc = $image[0];
        $display_title = get_the_title();
        wp_reset_postdata();
      endif ;

      if (get_sub_field('display_title')) {
        $display_title = get_sub_field('display_title');
      }

      if (get_sub_field('teaser_text')) {
				$teaser_text = get_sub_field('teaser_text');
				$teaser_text = wp_trim_words( $teaser_text, 13, '...' );
			}  
      
      $faqItems[] = array(
        'id' => $postId,
        'image' => $imageSrc,
        'title' => $display_title,
        'text' => $teaser_text,
        'link' => $postLink
      );

    endwhile;
  endif;
?>
<div id="faq-data" data-faqdata='<?php echo json_encode($faqItems, JSON_HEX_APOS); ?>'></div>

<?php

  // $contentblock = nl2br(get_the_content());

  $innerContent = escapeJsonString($contentblock);
  $secondaryContent = escapeJsonString(get_field('secondary_content'));

  $innerData = array(
    'title' => get_the_title(),
    'content' => get_the_content(),
    'image' => get_the_post_thumbnail_url(get_the_ID(),'full'),
    'secondary' => get_field('secondary_content'),
    'price' => $product->regular_price,
    'sale_price' => $product->sale_price
  );
?>
<div id="innerData" data-content='<?php echo json_encode($innerData, JSON_HEX_APOS); ?>'></div>

<?php
// get_posts in same custom taxonomy
$postlist_args = array(
  'posts_per_page'  => -1,
  'orderby'         => 'menu_order',
  'order'           => 'ASC',
  'post_type'       => 'product',
  'tax_query' => array(
    array(
      'taxonomy' => 'product_cat',
      'field' => 'slug',
      'terms' => 'home-theatre-packs'
    )
  )
); 
$postlist = get_posts( $postlist_args );

// get ids of posts retrieved from get_posts
$ids = array();
foreach ($postlist as $thepost) {
  $ids[] = $thepost->ID;
}

// get and echo previous and next post in the same taxonomy        
$thisindex = array_search($post->ID, $ids);
$previd = $ids[$thisindex-1];
$nextid = $ids[$thisindex+1];

if ( !empty($previd) ) {
  $prevContent = array(
    'title' => get_the_title( $previd ),
    'link' => get_permalink($previd),
    'image' => get_the_post_thumbnail_url($previd)
  );
}
?>
<script>
  var prevContent = JSON.parse('<?php echo json_encode($prevContent, JSON_HEX_APOS); ?>');
</script>
<?php
if ( !empty($nextid) ) {
  $nextContent = array(
    'title' => get_the_title( $nextid ),
    'link' => get_permalink($nextid),
    'image' => get_the_post_thumbnail_url($nextid)
  );
}
?>
<script>
  var nextContent = JSON.parse('<?php echo json_encode($nextContent, JSON_HEX_APOS); ?>');
</script>
<?php
  // Restore original Post Data
  wp_reset_postdata();
  wp_reset_query();
?>

<?php
  $args = array(
    'post_type'		=> 'accessories',
    'meta_key'		=> 'featured',
    'meta_value'	=> true
  );

  $the_query = new WP_Query( $args );
  
  if( $the_query->have_posts() ):
    while( $the_query->have_posts() ) : $the_query->the_post();
      $postId = get_the_ID();

      $accData = array(
        'acc_title' => get_the_title(),
        'acc_text' => get_field('teaser_content', $postId),
        'acc_link' => get_field('link_destination',$accPost),
        'acc_img' => get_field('image', $postId),
        'acc_bg' => get_field('background_image', $postId)
      );

    endwhile;
  endif;
?>
<div id="ctaData" data-content='<?php echo json_encode($accData, JSON_HEX_APOS); ?>'></div>
<?php wp_reset_query(); ?>
<div id="detail"></div>


<?php get_footer('hifi_packages'); ?>

<?php
  $appjs = '/assets/components/js/htp_inner.js';
?>
<script src="<?php echo get_stylesheet_directory_uri() . $appjs . $appmodDate ?>" charset="utf-8" async></script>
<?php else : ?>
    <div class="center-site">
	<?php
/**
 * woocommerce_before_main_content hook.
 *
 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
 * @hooked woocommerce_breadcrumb - 20
 */
do_action('woocommerce_before_main_content');
?>

		<?php while (have_posts()): the_post();?>

				<?php wc_get_template_part('content', 'single-product');?>

			<?php endwhile; // end of the loop. ?>

	<?php
/**
 * woocommerce_after_main_content hook.
 *
 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
 */
do_action('woocommerce_after_main_content');
?>
    </div>
	<?php
/**
 * woocommerce_sidebar hook.
 *
 * @hooked woocommerce_get_sidebar - 10
 */
//do_action('woocommerce_sidebar');
?>
<script>
  <?php
$brands = wp_get_post_terms($post->ID, 'product_brand', array("fields" => "all"));
$topcat = '';

$categories = get_the_terms($post->ID, 'product_cat');
foreach ($categories as $cat) {
    if ($cat->parent == 0) {
        $topcat = $cat->name;
    }
}
?>

  jQuery(document).ready(function($){


  	ga('require', 'ec');
  	ga('ec:addProduct', {            // Provide product details in an impressionFieldObject.
      'id': '<?php echo $product->id; ?>',                   // Product ID (string).
      'name': '<?php echo $product->post->post_title; ?>',
      'category': '<?php echo $topcat; ?>',
      'brand': '<?php echo $brands[0]->name; ?>',
      'price': '<?php echo $product->get_price(); ?>',

    });
    ga('ec:setAction', 'detail');
  })

</script>
<?php endif; ?>
<?php get_footer('shop');

/* Omit closing PHP tag at the end of PHP files to avoid "headers already sent" issues. */
