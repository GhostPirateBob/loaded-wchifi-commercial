<?php
?><h1>awdawdawdawaw</h1><?php
  get_header(); 
  
  // global app variables
  $baseUrl = site_url();
  $product = new WC_Product( get_the_ID() );
  
  $globalData = array(
    'baseUrl' => $baseUrl
  )
?>
<div id="app-globals" data-appdata='<?php echo json_encode($globalData, JSON_HEX_APOS); ?>'></div>
<?php
  $appcss = '/assets/components/css/htp_inner.css';
  $appmodDate = '?v=' . (filemtime(get_stylesheet_directory() . $appcss));
?>
<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri() . $appcss . $appmodDate ?>" type="text/css" charset="utf-8">

<?php
  $faqItems = array();

  if( have_rows('faq_posts', 13834) ):
    while ( have_rows('faq_posts', 13834) ) : the_row();
      $post = get_sub_field('link');

      if ($post) :
        setup_postdata( $post );
        $postLink = get_the_permalink();
        $postId = get_the_ID();
        $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), array(250,166) );
        $imageSrc = $image[0];
        $display_title = get_the_title();
        wp_reset_postdata();
      endif ;

      if (get_sub_field('display_title')) {
        $display_title = get_sub_field('display_title');
      }

      if (get_sub_field('teaser_text')) {
				$teaser_text = get_sub_field('teaser_text');
				$teaser_text = wp_trim_words( $teaser_text, 13, '...' );
			}  
      
      $faqItems[] = array(
        'id' => $postId,
        'image' => $imageSrc,
        'title' => $display_title,
        'text' => $teaser_text,
        'link' => $postLink
      );

    endwhile;
  endif;
?>
<div id="faq-data" data-faqdata='<?php echo json_encode($faqItems, JSON_HEX_APOS); ?>'></div>

<?php

  // $contentblock = nl2br(get_the_content());

  $innerContent = escapeJsonString($contentblock);
  $secondaryContent = escapeJsonString(get_field('secondary_content'));

  $innerData = array(
    'title' => get_the_title(),
    'content' => get_the_content(),
    'image' => get_the_post_thumbnail_url(get_the_ID(),'full'),
    'secondary' => get_field('secondary_content'),
    'price' => $product->regular_price,
    'sale_price' => $product->sale_price
  );
?>
<div id="innerData" data-content='<?php echo json_encode($innerData, JSON_HEX_APOS); ?>'></div>

<?php
// get_posts in same custom taxonomy
$postlist_args = array(
  'posts_per_page'  => -1,
  'orderby'         => 'menu_order',
  'order'           => 'ASC',
  'post_type'       => 'product',
  'tax_query' => array(
    array(
      'taxonomy' => 'product_cat',
      'field' => 'slug',
      'terms' => 'home-theatre-packs'
    )
  )
); 
$postlist = get_posts( $postlist_args );

// get ids of posts retrieved from get_posts
$ids = array();
foreach ($postlist as $thepost) {
  $ids[] = $thepost->ID;
}

// get and echo previous and next post in the same taxonomy        
$thisindex = array_search($post->ID, $ids);
$previd = $ids[$thisindex-1];
$nextid = $ids[$thisindex+1];

if ( !empty($previd) ) {
  $prevContent = array(
    'title' => get_the_title( $previd ),
    'link' => get_permalink($previd),
    'image' => get_the_post_thumbnail_url($previd)
  );
}
?>
<script>
  var prevContent = JSON.parse('<?php echo json_encode($prevContent, JSON_HEX_APOS); ?>');
</script>
<?php
if ( !empty($nextid) ) {
  $nextContent = array(
    'title' => get_the_title( $nextid ),
    'link' => get_permalink($nextid),
    'image' => get_the_post_thumbnail_url($nextid)
  );
}
?>
<script>
  var nextContent = JSON.parse('<?php echo json_encode($nextContent, JSON_HEX_APOS); ?>');
</script>
<?php
  // Restore original Post Data
  wp_reset_postdata();
  wp_reset_query();
?>

<?php
  $args = array(
    'post_type'		=> 'accessories',
    'meta_key'		=> 'featured',
    'meta_value'	=> true
  );

  $the_query = new WP_Query( $args );
  
  if( $the_query->have_posts() ):
    while( $the_query->have_posts() ) : $the_query->the_post();
      $postId = get_the_ID();

      $accData = array(
        'acc_title' => get_the_title(),
        'acc_text' => get_field('teaser_content', $postId),
        'acc_link' => get_field('link_destination',$accPost),
        'acc_img' => get_field('image', $postId),
        'acc_bg' => get_field('background_image', $postId)
      );

    endwhile;
  endif;
?>
<div id="ctaData" data-content='<?php echo json_encode($accData, JSON_HEX_APOS); ?>'></div>
<?php wp_reset_query(); ?>
<div id="detail"></div>


<?php get_footer('hifi_packages'); ?>

<?php
  $appjs = '/assets/components/js/htp_inner.js';
?>
<script src="<?php echo get_stylesheet_directory_uri() . $appjs . $appmodDate ?>" charset="utf-8" async></script>


<?php
get_footer();
