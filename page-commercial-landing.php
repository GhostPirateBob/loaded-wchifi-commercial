<?php
/**
* Template Name: Commercial Landing
*
*/

get_header();

?>

<div id="primary" class="content-area">

<?php // check if the flexible content field has rows of data
  
  if( have_rows('commercial_page_content') ):

    // loop through the rows of data
    while ( have_rows('commercial_page_content') ) : the_row();

      if ( get_row_layout() === 'rollover_box_section' ) : ?>

  <div class="container commercial__page-container">
    <div class="row">
      <?php if ( have_rows( 'rollover_box_Items' ) ) : ?>
        <?php while ( have_rows( 'rollover_box_Items' ) ) : the_row(); ?>
          <?php if ( have_rows( 'rollover_item' ) ) : ?>
            <?php while ( have_rows( 'rollover_item' ) ) : the_row(); ?>
              <div class="col-12 col-lg-6 col-xl-4 gutters mb-4">
              <?php if (get_sub_field( 'page_link' )) { ?>
                <a href="<?php the_sub_field( 'page_link' ); ?>" class="rollover-a">
              <?php } ?>
                <div class="wc-rollover-box">
                  <?php if ( get_sub_field( 'background_image' ) ) { ?>
                  <img src="<?php the_sub_field( 'background_image' ); ?>"
                    class="wc-commercial-overlay-img img-fluid">
                  <?php } ?>
                  <div class="wc-rollover-caption">
                    <?php if ( get_sub_field( 'icon' ) ) { ?>
                    <span class="mt-1 mb-1 wc-rollover-icon">
                      <img src="<?php the_sub_field( 'icon' ); ?>" class="img-fluid wc-rollover-icon-img">
                    </span>
                    <?php } ?>
                    <h2 class="mt-1 mb-1"> <?php the_sub_field( 'heading' ); ?> </h2>
                    <p class="mt-1 mb-1"> <?php the_sub_field( 'content' ); ?> </p>
                  </div>
                </div>
                </a>
              </div>
            <?php endwhile; ?>
          <?php endif; ?>
        <?php endwhile; ?>
      <?php endif; ?>
      </div>
    </div>

    <?php elseif ( get_row_layout() == 'brands_section' ) : ?>
      <?php $images_images = get_sub_field( 'images' ); ?>
      <?php if ( $images_images ) :  ?>
        <div class="wrapper commercial__page-container">
          <div class="container">
            <div class="row brands-content-row">
              <div class="col-12">
                <h3 class="feature__heading__h3"><?php echo get_sub_field('heading'); ?></h3>
                <?php the_sub_field( 'subheading' ); ?>
              </div>
            </div>
            <div class="row flex-justify-evenly mt-5">
        <?php foreach ( $images_images as $images_image ): ?>
              <div class="col-auto gutters">
                <a href="<?php echo $images_image['url']; ?>">
                  <img src="<?php echo $images_image['sizes']['thumbnail']; ?>" alt="<?php echo $images_image['alt']; ?>" />
                </a>
              </div>
        <?php endforeach; ?>
            </div>
          </div>
        </div>
      <?php endif; ?>
    <?php elseif ( get_row_layout() == 'cta_image_section' ) : ?>
        <div class="wrapper px-0 pt-3 pb-1 cta-block-wrapper">
          <div class="container">
            <div class="row flex-justify-evenly">
              <?php if ( get_sub_field( 'item_one' ) ) { ?>
                <div class="col-auto">
                  <img src="<?php the_sub_field( 'item_one' ); ?>" />
                </div>
              <?php } ?>
              <?php if ( get_sub_field( 'item_two' ) ) { ?>
                <div class="col-auto">
                  <img src="<?php the_sub_field( 'item_two' ); ?>" />
                </div>
              <?php } ?>
              <?php if ( get_sub_field( 'item_three' ) ) { ?>
                <div class="col-auto">
                  <img src="<?php the_sub_field( 'item_three' ); ?>" />
                </div>
              <?php } ?>
            </div>
          </div>
        </div>
    <?php elseif ( get_row_layout() == 'two_column_box_section' ) : ?>
    <div class="wrapper two-column-wrapper">
      <div class="row no-gutters">
      <?php if ( have_rows( 'item_one' ) ) : ?>
        <?php while ( have_rows( 'item_one' ) ) : the_row(); ?>
          <div class="col-12 col-lg-6">
            <div class="wc-rollover-box wc-rollover-box-logo">
              <?php if ( get_sub_field( 'background' ) ) { ?>
              <img src="<?php the_sub_field( 'background' ); ?>"
                class="wc-commercial-overlay-img img-fluid">
              <?php } ?>
              <div class="wc-rollover-caption">
                <?php if ( get_sub_field( 'logo' ) ) { ?>
                <span class="mt-1 mb-1 wc-rollover-icon">
                  <img src="<?php the_sub_field( 'logo' ); ?>" class="img-fluid wc-rollover-icon-logo">
                </span>
                <?php } ?>
              </div>
            </div>
          </div>
        <?php endwhile; ?>
      <?php endif; ?>
      <?php if ( have_rows( 'item_two' ) ) : ?>
        <?php while ( have_rows( 'item_two' ) ) : the_row(); ?>
          <div class="col-12 col-lg-6">
            <div class="wc-rollover-box wc-rollover-box-logo">
              <?php if ( get_sub_field( 'background' ) ) { ?>
              <img src="<?php the_sub_field( 'background' ); ?>"
                class="wc-commercial-overlay-img img-fluid">
              <?php } ?>
              <div class="wc-rollover-caption">
                <?php if ( get_sub_field( 'logo' ) ) { ?>
                <span class="mt-1 mb-1 wc-rollover-icon">
                  <img src="<?php the_sub_field( 'logo' ); ?>" class="img-fluid wc-rollover-icon-logo">
                </span>
                <?php } ?>
              </div>
            </div>
          </div>
        <?php endwhile; ?>
        </div>
      </div>
      <?php endif; ?>

    <?php elseif ( get_row_layout() == 'cta_block_section' ) : ?>

    <div class="wrapper px-0 cta-block-wrapper">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <h2 class="mt-5 mb-5"><?php the_sub_field( 'blockquote' ); ?></h2>
          </div>
        </div>
      </div>
    </div>

      <?php
      
      elseif ( get_row_layout() === 'heading_text_section' ) : ?>

      <div class="container-fluid px-0">
        <div
          class="row feature__heading__text__section<?php $background_color = get_sub_field('background_color'); if ( $background_color == 'navy' ) { $background_class = 'feature__bg__navy'; $text_class = 'feature__text__light'; } else if ( $background_color == 'gray' ) { $background_class = 'feature__bg__gray'; $text_class = 'feature__text_dark'; } else if ( $background_color == 'white' ) { $background_class = 'feature__bg__white'; $text_class = 'feature__text__dark'; } echo ' '.$background_class; echo ' '.$text_class; ?>">
          <div class="container">
            <div class="row">
              <div class="col-12 col-lg-6 feature__gutters">
                <h2 class="feature__heading__h2"><?php echo get_sub_field('large_heading'); ?></h2>
              </div>
              <div class="col-12 col-lg-6 feature__gutters">
                <?php echo get_sub_field('text_block'); ?>
                <?php $read_more = get_sub_field('read_more'); if ( $read_more ): ?>
                    <a class="feature__read__more" href="<?php echo $read_more['url']; ?>" <?php echo $read_more['target']; ?> > 
                        <?php echo $read_more['text']; ?>
                    </a>
                <?php endif; ?>
              </div>
            </div>
          </div>
        </div>
      </div>

      <?php
      elseif( get_row_layout() === 'slider_section' ) : ?>

      <?php if ( get_sub_field('revolution_slider') ) { ?>
      <div class="container-fluid px-0">
        <div class="row row-commercial-slider">
          <?php $revoSlider = get_sub_field('revolution_slider') ; ?>
          <div class="col-12"><?php do_shortcode('[rev_slider alias="'. $revoSlider .'"]');?></div>
        </div>
      </div>
      <?php } ?>
      <?php

      elseif( get_row_layout() === 'image_text_section' ): ?>

      <div class="container-fluid px-0">
        <div
          class="row feature__image__text__section<?php $background_color = get_sub_field('background_color'); if ( $background_color == 'navy' ) { $background_class = 'feature__bg__navy'; $text_class = 'feature__text__light'; } else if ( $background_color == 'gray' ) { $background_class = 'feature__bg__gray'; $text_class = 'feature__text_dark'; } else if ( $background_color == 'white' ) { $background_class = 'feature__bg__white'; $text_class = 'feature__text__dark'; } echo ' '.$background_class; echo ' '.$text_class; ?>">
          <div class="container">
            <div class="row align-items-center">
              <div class="col-12 col-lg-6 feature__gutters<?php if (get_sub_field('text_position') == 'right') { echo ' feature__order__1'; } ?>">
                <h3 class="feature__heading__h3"><?php echo get_sub_field('heading'); ?></h3>
                <?php echo get_sub_field('text_block'); ?>
                <?php $read_more = get_sub_field('read_more'); if ( $read_more ): ?>
                    <a class="feature__read__more" href="<?php echo $read_more['url']; ?>" <?php echo $read_more['target']; ?> > 
                        <?php echo $read_more['text']; ?>
                    </a>
                <?php endif; ?>
                <?php $anchor = get_sub_field('anchor'); if( $anchor ): 
	$anchor_url = $anchor['url'];
	$anchor_title = $anchor['title'];
	$anchor_target = $anchor['target'] ? $anchor['target'] : '_self';?>
	<a class="feature__read__more" href="<?php echo esc_url($anchor_url); ?>" target="<?php echo esc_attr($anchor_target); ?>"><?php echo esc_html($anchor_title); ?></a>
<?php endif; ?> 

              </div>
              <?php $images = get_sub_field('images');
              $images_count = count($images); ?>
              <div
                class="col-12 col-lg-6 feature__image__holder feature__gutters<?php if (get_sub_field('text_position') == 'right') { echo ' feature__order__0'; } if ($images_count > 1) { echo ' feature__slider__init'; } ?>">
                <?php if( $images ): ?>
                <?php foreach( $images as $image ): ?>
                <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" class="d-block w-100 img-fluid" />
                <?php endforeach; ?>
                <?php endif; ?>
              </div>
            </div>
          </div>
        </div>
      </div>
      <?php if ($images_count > 1) { ?>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js"></script>
      <script>
      jQuery( document ).ready( function () {
        jQuery( '.feature__slider__init' ).slick( {
          slidesToShow: 1,
          slidesToScroll: 1,
          dots: true,
          arrows: false,
          autoplay: true,
          autoplaySpeed: 4000,
        } );
      } );
    </script>
      <?php } ?>

      <?php
      
      elseif( get_row_layout() === 'image_background_section' ): ?>

      <div class="container-fluid px-0">
        <div
          class="row feature__image__background__section<?php if (get_sub_field('text_color') == 'light') { $text_class = 'feature__text__light'; } else { $text_class = 'feature__text__dark'; } echo ' '.$text_class; ?>"
          style="background-image: url('<?php $background_image = get_sub_field('background_image'); echo $background_image['url']; ?>')">
          <div class="container">
            <div class="row align-items-start">
              <?php if (get_sub_field('text_position') !== 'center' ) : ?>
              <div class="col-12 col-lg-6 feature__gutters<?php if (get_sub_field('text_position') == 'right') { echo ' feature__order__1'; } ?>">
                <?php echo get_sub_field('text_block'); ?>
                <?php $read_more = get_sub_field('read_more'); if ( $read_more ): ?>
                    <a class="feature__read__more" href="<?php echo $read_more['url']; ?>" <?php echo $read_more['target']; ?> > 
                        <?php echo $read_more['text']; ?>
                    </a>
                <?php endif; ?>
              </div>
              <div
                class="col-12 col-lg-6 feature__image__holder feature__gutters<?php if (get_sub_field('text_position') == 'right') { echo ' feature__order__0'; } ?>">
                <h3 class="feature__heading__h3"><?php echo get_sub_field('heading'); ?></h3>
              </div>
              <?php else : ?>
              <div class="col-12 col-lg-2"></div>
              <div
                class="col-12 col-lg-8 justify-self-center feature__centered__content feature__gutters<?php if (get_sub_field('text_position') == 'right') { echo ' feature__order__1'; } ?>">
                <h3 class="feature__heading__h3"><?php echo get_sub_field('heading'); ?></h3>
                <?php echo get_sub_field('text_block'); ?>
                <?php $read_more = get_sub_field('read_more'); if ( $read_more ): ?>
                    <a class="feature__read__more" href="<?php echo $read_more['url']; ?>" <?php echo $read_more['target']; ?> > 
                        <?php echo $read_more['text']; ?>
                    </a>
                <?php endif; ?>
              </div>
              <div class="col-12 col-lg-2"></div>
              <?php endif; ?>
            </div>
          </div>
        </div>
      </div>

        <?php 
        elseif( get_row_layout() === 'google_map_section' ) : ?>
        
<?php
$locationQuery = new WP_Query(array(
    'post_type' => 'location',
    'post_status' => 'publish',
    'orderby' => 'date',
    'order' => 'DESC',
    'posts_per_page' => -1
));

while ($locationQuery->have_posts()) {
    $locationQuery->the_post();
    $storeID = get_the_ID();
    $stores[$storeID]['id'] = $storeID;
    $stores[$storeID]['title'] = get_the_title();
    $stores[$storeID]['permalink'] = get_the_permalink();
    if (get_field('marker_image')) {
      $stores[$storeID]['marker'] = get_field('marker_image');
    } else {
      $stores[$storeID]['marker'] = 'https://www.westcoasthifi.com.au/wp-content/themes/wchifi-2017/assets/images/map-icon-shopping.svg';
    }
    $stores[$storeID]['infoboxHeading'] = get_field('map_info_heading');
    $stores[$storeID]['infoboxContent'] = get_field('map_info_content');
    $stores[$storeID]['cidCode'] = get_field('cid');
    if ( get_field('cid') ) {
      $stores['mapsLink'] = "http://maps.google.com/maps/place?cid=".$cidCode;
    } elseif ( get_field( 'view_on_google_maps_link' ) ) {
      $stores['mapsLink'] = get_field( 'view_on_google_maps_link' );
    }
    $stores[$storeID]['directionsLink'] = get_field('link_directions');
    $stores[$storeID]['gmap'] = get_field('google_map_location');
}
wp_reset_query();
?>

    <div class="container-fluid px-0">
      <div class="row feature__text__dark">
        <div class="col-12 col-xl-6 page-contact-blue-rectangle py-5">
          <h3 class="feature__heading__h3 mt-5 mb-2"><?php the_sub_field( 'heading' ); ?></h3>
          <p class="mb-5"> <?php the_sub_field( 'content' ); ?> </p>
        </div>
        <div id="map" class="col-12 col-xl-6 page-contact-gmap"></div>
      </div>
    </div>

    <script type="text/javascript">
function initMap() {

// Set the target for firing up the map, usually "#map"
    var mapElement = document.getElementById('map');

// Map Options
    var mapOptions = {
        zoom: 10,
        disableDefaultUI: true,
        center: new google.maps.LatLng(<?php echo get_sub_field('lat'); ?>,<?php echo get_sub_field('lng'); ?>),
        // styles: [ { "featureType": "administrative", "elementType": "all", "stylers": [ { "visibility": "on" }, { "saturation": -100 }, { "lightness": 20 } ] }, { "featureType": "landscape.man_made", "elementType": "all", "stylers": [ { "visibility": "simplified" }, { "saturation": -60 }, { "lightness": 10 } ] }, { "featureType": "landscape.natural", "elementType": "all", "stylers": [ { "visibility": "simplified" }, { "saturation": -60 }, { "lightness": 60 } ] }, { "featureType": "poi", "elementType": "all", "stylers": [ { "visibility": "off" }, { "saturation": -100 }, { "lightness": 60 } ] }, { "featureType": "road", "elementType": "all", "stylers": [ { "visibility": "on" }, { "saturation": -100 }, { "lightness": 40 } ] }, { "featureType": "transit", "elementType": "all", "stylers": [ { "visibility": "off" }, { "saturation": -100 }, { "lightness": 60 } ] }, { "featureType": "water", "elementType": "all", "stylers": [ { "visibility": "on" }, { "saturation": -10 }, { "lightness": 30 }, { "color": "#739cd4" } ] } ]
    };

    var map = new google.maps.Map(mapElement, mapOptions);

<?php
$markerCounter = 0; 
foreach ($stores as $store) { 
  if ( $markerCounter !== 1 ) { ?>
        var marker<?php echo $markerCounter; ?> = new google.maps.Marker({
          position: new google.maps.LatLng(<?php echo $store['gmap']['lat']; ?>,<?php echo $store['gmap']['lng']; ?>),
          map: map,
          title: '<?php echo $store['title']; ?>',
          icon: '<?php echo $store['marker']; ?>'
        });
        var contentString<?php echo $markerCounter; ?> = `
        <div class="gmap-info-body" style="max-width: 268px;">
            <h3 class="gmap-info-title">
                <?php echo $store['infoboxHeading']; ?>
            </h3>
            <div class="card-text">
                <?php echo $store['infoboxContent']; ?>
            </div>
            <a href="<?php echo $store['directionsLink']; ?>" class="card-link mr-2" target="_blank">
                Get Directions
                <i class="fa fa-external-link" aria-hidden="true"></i>
            </a>
            <a href="<?php echo $store['mapsLink']; ?>" class="card-link" target="_blank">
                View on Google Maps
                <i class="fa fa-map-marker" aria-hidden="true"></i>
            </a>
        </div>`;
        var infowindow<?php echo $markerCounter; ?> = new google.maps.InfoWindow({
            content: contentString<?php echo $markerCounter; ?>
        });
        marker<?php echo $markerCounter; ?>.addListener('click', function() {
            infowindow<?php echo $markerCounter; ?>.open(map, marker<?php echo $markerCounter; ?>);
        });
<?php
      }
    $markerCounter++; 
  }
?>
    }
</script>
<script type="text/javascript" 
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD8zcpxFEErbOfKxCuAFXey8AJIZTiUgVg&callback=initMap" 
        async 
        defer ></script>
    <?php
      endif;
    endwhile;
  else : 
  ?>

    <p style="margin: 100px 0px; text-align: center;">No page content found.</p>

  <?php endif; ?>

    </div><!-- #primary -->

    <div id="htp_enquiry-form-wrapper">
      <div class="container">
        <div class="row">
          <div class="col-12">
          <h3 class="feature__heading__h3"><?php echo get_field('form_content_heading'); ?></h3>
          <?php echo get_field('form_content_content'); ?>
          <?php gravity_form( 10, false, true, false, null, true, 10000, true ); ?>
          </div>
        </div>
      </div>
    </div>
    <script src="<?php echo get_stylesheet_directory_uri();?>/assets/vendor/jquery.customSelect.min.js" charset="utf-8"></script>
    <script>
      jQuery( document ).ready( function () {
        jQuery( document ).bind( 'gform_post_render', function () {
          jQuery( '#input_3_5' ).customSelect();
          jQuery( '#input_6_5' ).customSelect();
        } );
      } );
    </script>

    <?php
get_footer();