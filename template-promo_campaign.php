<?php

  // * Template Name: Promo Campaign

get_header(); ?>

  <div class="banner-container">
  <?php /* $slider = get_field('slider_code'); ?>
  <?php if ( $slider == '[sliderpro id="7"]' ) { ?>
	<div id="slider__7__container" class="container-fluid">
		<div class="row">
			<div class="container slider__container">
				<div id="slider__7__replace" class="row">
					<div class="col-12 col-md-6 align-self-center">
						<h1 style="color:#fff;font-style:italic;LINE-HEIGHT:45px;font-size: 54px;font-family: 'MiloPro-BoldIta','Ubuntu','lucida grande','lucida sans unicode','lucida sans',lucida,sans-serif;font-weight: bold;">MEGA DEALS ACROSS<br> THE BEST BRANDS</h1>			
						<div style="white-space:normal !important;">
							<p style="color:#fff; font-style:italic;">Massive savings across SONOS, BOSE, PANASONIC and more. Demo in-store with your favourite movies and music before you buy.
						</div>
					</div>
					<div class="col-12 col-md-6 align-self-end">
						<img src="<?php echo get_site_url(); ?>/wp-content/uploads/2018/02/banner3.jpg">
					</div>
				</div>
			</div>
		</div>
	</div>
  <?php } else {
	  echo do_shortcode($slider);
  } */ ?>
  <?php if ( get_field('revolution_slider') ) { ?>
    <?php $revoSlider = get_field('revolution_slider'); ?>
    <?php do_shortcode('[rev_slider alias="'. $revoSlider .'"]'); ?>
    <?php }?> 
  
  </div>

  <div class="promocampaign-highlight">
    <?php the_field('highlight_text'); ?>
  </div>

	<div class="center-site woocommerce">
      <?php

      /*
      *  Loop through post objects (assuming this is a multi-select field) ( setup postdata )
      *  Using this method, you can use all the normal WP functions as the $post object is temporarily initialized within the loop
      *  Read more: http://codex.wordpress.org/Template_Tags/get_posts#Reset_after_Postlists_with_offset
      */

    $sale_items = get_field('sale_items');

    if( $sale_items ): ?>
        <ul class="products">
        <?php foreach( $sale_items as $post) : ?>
            <?php setup_postdata( $post ); ?>
            <?php if ( get_post_status() === 'publish' ) { ?>
            <div class="flex--item">
            <?php wc_get_template_part( 'content', 'product' ); ?>
            </div>
            <?php } ?>
        <?php endforeach; ?>
        </ul>
        <?php wp_reset_postdata(); ?>
    <?php endif; ?>

	</div><!-- .center-site -->

<script>
// 	ga('send', 'event', 'Promotions', 'Store Specials', '<?php the_title() ?>');
</script>

<div class="cta-blocks">
  <div class="cta-item" id="cta-pricematch">
    <div class="overlay">
      <div class="media-container">
        <a href="<?php bloginfo('url'); ?>/pricematch"><?php echo file_get_contents(get_stylesheet_directory_uri() . "/assets/images/cta-pricematch.svg") ?></a>
      </div>
      <a href="<?php bloginfo('url'); ?>/pricematch" class="ci-button">Learn More ></a>
    </div>
  </div>
  <div class="cta-item" id="cta-store">
    <div class="overlay">
      <div class="media-container">
        <a href="<?php bloginfo('url'); ?>/store-locations/"><?php echo file_get_contents(get_stylesheet_directory_uri() . "/assets/images/nearest-store.svg") ?></a>
      </div>
      <a href="<?php bloginfo('url'); ?>/store-locations/" class="ci-button">Find a store ></a>
    </div>
  </div>
  <div class="cta-item" id="cta-clearance">
    <div class="overlay">
      <div class="media-container">
        <a href="<?php bloginfo('url'); ?>/clearance-centre/"><?php echo file_get_contents(get_stylesheet_directory_uri() . "/assets/images/cta-clearance.svg") ?></a>
      </div>
      <a href="<?php bloginfo('url'); ?>/clearance-centre/" class="ci-button">Grab a bargain ></a>
    </div>
  </div>
</div>


<?php
get_footer();

