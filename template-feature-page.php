<?php
/**
* Template Name: Feature Page
*
*/

get_header(); ?>

  <div id="primary" class="content-area">
  <?php // check if the flexible content field has rows of data
  
  if( have_rows('page_content') ):

    // loop through the rows of data
    while ( have_rows('page_content') ) : the_row();

      if( get_row_layout() == 'header_section' ): ?>

      <div class="container-fluid px-0">
      <?php if ( get_sub_field('image_type') == 'cover' ) { ?>
        <div class="row align-items-center feature__header__section" 
          style="background-image: url('<?php 
          $background_image = get_sub_field('background_image'); 
          echo $background_image['url']; 
          ?>');">
      <?php } else if ( get_sub_field('image_type') == 'contain' && get_sub_field('background_image') ) { ?>
        <div class="row align-items-center justify-content-center feature__header__image">
          <div class="col-auto">
            <img class="feature__header__img img-fluid pt-4 pb-4" src="<?php 
              $background_image = get_sub_field('background_image'); 
              echo $background_image['url']; ?>" />
          </div>
      <?php } ?>
          <?php if ( get_sub_field('heading') ) { ?>
          <div class="col-12 pb-4">
            <h1 class="feature__header__h1"<?php 
            if ( get_sub_field('text_color') == 'white') {
              echo ' style="color: #fff;"';
            } else if ( get_sub_field('text_color') == 'black') {
              echo ' style="color: #000;"';
            } else if ( get_sub_field('text_color') == 'navy') {
              echo ' style="color: #3B4868;"';
            } ?>><?php echo get_sub_field('heading'); ?></h1>
          </div>
          <?php } ?>
        </div>
      </div>
      <?php

      elseif( get_row_layout() == 'heading_text_section' ): ?>

        <div class="container-fluid px-0">
          <div class="row feature__heading__text__section<?php $background_color = get_sub_field('background_color'); if ( $background_color == 'navy' ) { $background_class = 'feature__bg__navy'; $text_class = 'feature__text__light'; } else if ( $background_color == 'gray' ) { $background_class = 'feature__bg__gray'; $text_class = 'feature__text_dark'; } else if ( $background_color == 'white' ) { $background_class = 'feature__bg__white'; $text_class = 'feature__text__dark'; } echo ' '.$background_class; echo ' '.$text_class; ?>">
            <div class="container">
              <div class="row">
                <div class="col-12 col-lg-6 feature__gutters">
                  <h2 class="feature__heading__h2"><?php echo get_sub_field('large_heading'); ?></h2>
                </div>
                <div class="col-12 col-lg-6 feature__gutters">
                  <?php echo get_sub_field('text_block'); ?>
                  <?php if (get_sub_field('read_more_link')) { ?>
                  <a class="feature__read__more" href="<?php echo get_sub_field('read_more_link'); ?>"><?php echo get_sub_field('read_more_text'); ?></a>
                  <?php } ?>
                </div>
              </div>
            </div>
          </div>
        </div>
      
      <?php
        
      elseif( get_row_layout() == 'image_text_section' ): ?>
      
      <div class="container-fluid px-0">
        <div class="row feature__image__text__section<?php $background_color = get_sub_field('background_color'); if ( $background_color == 'navy' ) { $background_class = 'feature__bg__navy'; $text_class = 'feature__text__light'; } else if ( $background_color == 'gray' ) { $background_class = 'feature__bg__gray'; $text_class = 'feature__text_dark'; } else if ( $background_color == 'white' ) { $background_class = 'feature__bg__white'; $text_class = 'feature__text__dark'; } echo ' '.$background_class; echo ' '.$text_class; ?>">
          <div class="container">
            <div class="row align-items-center">
              <div class="col-12 col-lg-6 feature__gutters<?php if (get_sub_field('text_position') == 'right') { echo ' feature__order__1'; } ?>">
                <h3 class="feature__heading__h3"><?php echo get_sub_field('heading'); ?></h3>
                <?php echo get_sub_field('text_block'); ?>
              </div>
              <?php $images = get_sub_field('images');
              $images_count = count($images); ?>
              <div class="col-12 col-lg-6 feature__image__holder feature__gutters<?php if (get_sub_field('text_position') == 'right') { echo ' feature__order__0'; } if ($images_count > 1) { echo ' feature__slider__init'; } ?>">
                <?php if( $images ): ?>
                    <?php foreach( $images as $image ): ?>
                      <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                    <?php endforeach; ?>
                <?php endif; ?>
              </div>
            </div>
          </div>
        </div>
      </div>
      
      <?php
      
      elseif( get_row_layout() == 'image_background_section' ): ?>
      
      <div class="container-fluid px-0">
        <div class="row feature__image__background__section<?php if (get_sub_field('text_color') == 'light') { $text_class = 'feature__text__light'; } else { $text_class = 'feature__text__dark'; } echo ' '.$text_class; ?>" style="background-image: url('<?php $background_image = get_sub_field('background_image'); echo $background_image['url']; ?>')">
          <div class="container">
            <div class="row align-items-center">
              <?php if (get_sub_field('text_position') !== 'center' ) : ?>
              <div class="col-12 col-lg-6 feature__gutters<?php if (get_sub_field('text_position') == 'right') { echo ' feature__order__1'; } ?>">
                <h3 class="feature__heading__h3"><?php echo get_sub_field('heading'); ?></h3>
                <?php echo get_sub_field('text_block'); ?>
              </div>
              <div class="col-12 col-lg-6 feature__image__holder feature__gutters<?php if (get_sub_field('text_position') == 'right') { echo ' feature__order__0'; } ?>">
              </div>
              <?php else : ?>
              <div class="col-12 col-lg-2"></div>
              <div class="col-12 col-lg-8 justify-self-center feature__centered__content feature__gutters<?php if (get_sub_field('text_position') == 'right') { echo ' feature__order__1'; } ?>">
                <h3 class="feature__heading__h3"><?php echo get_sub_field('heading'); ?></h3>
                <?php echo get_sub_field('text_block'); ?>
              </div>
              <div class="col-12 col-lg-2"></div>
              <?php endif; ?>
            </div>
          </div>
        </div>
      </div>

    <?php	endif;

    endwhile;

  else :

    echo '<p style="margin: 100px 0px; text-align: center;">No page content found.</p>';

  endif;

  ?>
      
  </div><!-- #primary -->
  <div id="htp_enquiry-form-wrapper">
    <div class="container">
    <?php gravity_form( 'Speak to an expert', $display_title = true, $display_description = true, $display_inactive = false, $field_values = null, $ajax = true, $tabindex, $echo = true ); ?>
    </div>
  </div>
  <script src="<?php echo get_stylesheet_directory_uri();?>/assets/vendor/jquery.customSelect.min.js" charset="utf-8"></script>
  <script>
    jQuery(document).ready(function(){
    jQuery(document).bind('gform_post_render', function(){
      jQuery('#input_3_5').customSelect();
      jQuery('#input_6_5').customSelect();
    });
    jQuery( '.feature__slider__init' ).each(function() {
      jQuery( this ).slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: true,
        arrows: false,
        autoplay: true,
        autoplaySpeed: 4000,
      });
    });
    });
  </script>

<?php
get_footer();
