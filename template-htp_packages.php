<?php

  // * Template Name: Home Theatre Packages

get_header(); 

  // global app variables
  $baseUrl = site_url();
  
  $globalData = array(
    'baseUrl' => $baseUrl
  )
?>
<div id="app-globals" data-appdata='<?php echo json_encode($globalData, JSON_HEX_APOS); ?>'></div>

<script src="https://unpkg.com/isotope-layout@3/dist/isotope.pkgd.js"></script>
<?php
  $appcss = '/assets/components/css/htp_index.css';
  $appmodDate = '?v=' . (filemtime(get_stylesheet_directory() . $appcss));
?>
<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri() . $appcss . $appmodDate ?>" type="text/css" charset="utf-8">

<?php
  $bannerContent = str_replace("\n", "", get_field('content'));
  $bannerData = array(
    'bannerUrl' => get_field('banner_image'),
    'content' => $bannerContent,
    'baseUrl' => $baseUrl
  )
?>
<div id="banner-data" data-bannerdata='<?php echo json_encode($bannerData, JSON_HEX_APOS); ?>'></div>

<?php
  $faqItems = array();

  if( have_rows('faq_posts') ):
    while ( have_rows('faq_posts') ) : the_row();
      $post = get_sub_field('link');

      if ($post) :
        setup_postdata( $post );
        $postLink = get_the_permalink();
        $postId = get_the_ID();
        $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), array(250,166) );
        $imageSrc = $image[0];
        $display_title = get_the_title();
        wp_reset_postdata();
      endif ;

      if (get_sub_field('display_title')) {
        $display_title = get_sub_field('display_title');
      }

      if (get_sub_field('teaser_text')) {
        $teaser_text = get_sub_field('teaser_text');
        $teaser_text = wp_trim_words( $teaser_text, 13, '...' );
      }  

      $faqItems[] = array(
        'id' => $postId,
        'image' => $imageSrc,
        'title' => $display_title,
        'text' => $teaser_text,
        'link' => $postLink
      );

    endwhile;
  endif;
?>
<div id="faq-data" data-faqdata='<?php echo json_encode($faqItems, JSON_HEX_APOS); ?>'></div>

<?php
$ctaBlock = array();

$packageSet = array();

// check if the flexible content field has rows of data
if( have_rows('packages_container') ):

     // loop through the rows of data
    while ( have_rows('packages_container') ) : the_row();

        if( get_row_layout() == 'package_set' ):

          if( have_rows('packages') ):
              // loop through the rows of data
              while ( have_rows('packages') ) : the_row();

              $post = get_sub_field('package');
              setup_postdata( $post );
              $product = new WC_Product( get_the_ID() );

              $packageSet[] = array(
                'id' => get_the_ID(),
                'type' => get_sub_field('type'),
                'name' => get_the_title(),
                'price' => $product->regular_price,
                'sale_price' => $product->sale_price,
                'permalink' => get_the_permalink(),
                'thumb' => get_the_post_thumbnail_url(get_the_ID(),'full'),
                'content' => $product->post->post_excerpt
              );

              wp_reset_postdata();

            endwhile;

            $accPost_object = get_sub_field('accessories_panel');
            $accPost = $accPost_object;

            setup_postdata( $accPost );
            if (get_field('image',$accPost) === false || strlen(get_field('image',$accPost)) < 12 ) {
              $accIMG = 'https://www.westcoasthifi.com.au/wp-content/themes/wchifi-2017/assets/images/banner-couch-01.png';
            } else {
              $accIMG = get_field('image',$accPost);
            }
            $ctaBlock = array(
              'acc_title' => get_the_title($accPost),
              'acc_text' => escapeJsonString(get_field('teaser_content',$accPost)),
              'acc_link' => get_field('link_destination',$accPost),
              'acc_img' => $accIMG,
              'acc_bg' => get_field('background_image',$accPost)
            );
            wp_reset_postdata();
            ?>
<div class="packages-list" id="packagelist_<?php echo $packageSet[0]['id'] ?>" data-packages='<?php echo json_encode($packageSet, JSON_HEX_APOS); ?>' data-accessories-panel='<?php echo json_encode($ctaBlock, JSON_HEX_APOS); ?>'></div>
          <?php            
          $packageSet = array();
          endif;

          // print_r( $packageSets);
        endif;
    endwhile;
else :

    // no layouts found

endif;
wp_reset_postdata();
?>

<?php 

// args
$args = array(
  'numberposts' => 1,
  'post_type'   => 'accessories',
  'meta_key'    => 'featured',
  'meta_value'  => true
);

$the_query = new WP_Query( $args );

if( $the_query->have_posts() ):
  while( $the_query->have_posts() ) : $the_query->the_post();
  $ctaBlock = array(
    'acc_title' => get_the_title(),
    'acc_text' => escapeJsonString(get_field('teaser_content', $accPost)),
    'acc_link' => get_the_permalink($accPost),
    'acc_img' => get_field('image', $accPost),
    'acc_bg' => get_field('background_image', $accPost)
  );
  endwhile;
endif;
wp_reset_query();

$packageSet = array();

// check if the flexible content field has rows of data
if( have_rows('packages_container') ):

     // loop through the rows of data
    while ( have_rows('packages_container') ) : the_row();

        if( get_row_layout() == 'package_set' ):

          $packageTypeBool = false;

          if( have_rows('packages') ):

              // loop through the rows of data
              while ( have_rows('packages') ) : the_row();

              $post = get_sub_field('package');
              setup_postdata( $post );
              $product = new WC_Product( get_the_ID() );

              if ( get_sub_field('type') !== 'none' ) {
                $packageTypeBool = true;
              }

              $packageSet[] = array(
                'id' => get_the_ID(),
                'type' => get_sub_field('type'),
                'name' => get_the_title(),
                'price' => $product->regular_price,
                'sale_price' => $product->sale_price,
                'permalink' => get_the_permalink(),
                'thumb' => get_the_post_thumbnail_url(get_the_ID(),'full'),
                'content' => $product->post->post_excerpt
              );


              wp_reset_postdata();

            endwhile;

          endif;

          // print_r( $packageSets);
        endif;

    endwhile;
    
    ?>
      <div id="allpackages" data-packages='<?php echo json_encode($packageSet, JSON_HEX_APOS); ?>' data-accessories-panel='<?php echo json_encode($ctaBlock, JSON_HEX_APOS); ?>'></div>
    <?php

else :

    // no layouts found

endif;
wp_reset_postdata();
?>

<div id="app" class="<?php
  if ( $packageTypeBool === false ) { 
    echo 'disable-filter-block'; } 
  ?>"></div>

<?php get_footer('hifi_packages'); ?>

<?php
  $appjs = '/assets/components/js/htp_index.js';
?>
<script src="<?php echo get_stylesheet_directory_uri() . $appjs . $appmodDate ?>" charset="utf-8" async></script>
<style type="text/css">
.disable-filter-block .FilterPanel__wrapper--wrjbE {
    display: none !important;
}
@media only screen and (min-width: 1024px) {
  .Banner__banner--iU22o {
    padding: 0 0 162px;
    display: flex;
    align-items: center;
    justify-content: center;
    flex-flow: column;
  }
}
</style>
<?php
get_footer();
