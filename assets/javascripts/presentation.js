(function($){

	$(document).ready(function(){

		// open external link in new tab/window
		// use rel="external" instead of target="_blank"
		$('a[rel="external"]').click( function() {
		  this.target = "_blank";
		});

		var menuButton = $('#menu-toggle');
		var menu = $('#masthead .menu-primary-navigation-container');

		menuButton.on('click', function(e){
			e.preventDefault();
			// var mastheadHeight = $('#masthead').height();
			$(this).toggleClass('is-active');
      menu.toggleClass('is-active');
      $('.nav-container.row').toggleClass('is-active');
			// menu.css('top',mastheadHeight);
		});

		$('.site-footer .menu-item-has-children > a').on('click', function(e){
  		e.preventDefault();
  		$(this).parents('li').toggleClass('active');
		});

	});

})(window.jQuery);
