<?php
add_action('wp_scheduled_delete', 'delete_expired_db_transients');

function delete_expired_db_transients() {

  global $wpdb, $_wp_using_ext_object_cache;

  if ($_wp_using_ext_object_cache) {
    return;
  }

  $time    = isset($_SERVER['REQUEST_TIME']) ? (int)$_SERVER['REQUEST_TIME'] : time();
  $expired = $wpdb->get_col("SELECT option_name FROM {$wpdb->options} WHERE option_name LIKE '_transient_timeout%' AND option_value < {$time};");

  foreach ($expired as $transient) {

    $key = str_replace('_transient_timeout_', '', $transient);
    delete_transient($key);
  }
}

function cptui_register_my_cpts() {

  /**
   * Post Type: Accessories.
   */

  $labels = array(
    "name"          => __("Accessories", "wchf2017"),
    "singular_name" => __("Accessory Block", "wchf2017"),
    "all_items"     => __("All Accessories Blocks", "wchf2017"),
  );

  $args = array(
    "label"               => __("Accessories", "wchf2017"),
    "labels"              => $labels,
    "description"         => "",
    "public"              => true,
    "publicly_queryable"  => true,
    "show_ui"             => true,
    "show_in_rest"        => false,
    "rest_base"           => "",
    "has_archive"         => false,
    "show_in_menu"        => true,
    "exclude_from_search" => false,
    "capability_type"     => "post",
    "map_meta_cap"        => true,
    "hierarchical"        => false,
    "rewrite"             => array("slug" => "accessories", "with_front" => true),
    "query_var"           => true,
    "supports"            => array("title", "editor", "thumbnail"),
  );

  register_post_type("accessories", $args);
}

add_action('init', 'cptui_register_my_cpts');

add_action('wp_enqueue_scripts', 'my_theme_enqueue_styles');
function my_theme_enqueue_styles() {
  wp_enqueue_style('parent-style', get_template_directory_uri() . '/style.css');
  wp_enqueue_style('wchf2017-style', get_stylesheet_directory_uri() . '/assets/stylesheets/css/wchf2017.css');
  wp_enqueue_style('bootstrap-4-grid', get_stylesheet_directory_uri() . '/assets/stylesheets/css/bootstrap-grid.min.css');
  wp_enqueue_script('presentation', get_stylesheet_directory_uri() . '/assets/javascripts/presentation.js', array('jquery'));
  wp_enqueue_style('jqmodalstyle', get_stylesheet_directory_uri() . '/assets/jquery-modal/jquery.modal.css');
  wp_enqueue_script('jqmodal', get_stylesheet_directory_uri() . '/assets/jquery-modal/jquery.modal.min.js', array('jquery'));
}

register_nav_menus(array(
  'footer-col-1' => __('Footer Column 1'),
  'footer-col-2' => __('Footer Column 2'),
  'footer-col-3' => __('Footer Column 3'),
  'footer-col-4' => __('Footer Column 4'),
));

//   add_filter( 'template_include', 'so_25789472_template_include' );

//   function so_25789472_template_include( $template ) {
//     if ( is_singular('product') && (has_term( 'home-theatre-packs', 'product_cat')) ) {
//       $template = get_stylesheet_directory() . '/woocommerce/single-product-home-theatre-packs.php';
//     }
//     return $template;
//   }

function escapeJsonString($value) {
  # list from www.json.org: (\b backspace, \f formfeed)
  $escapers     = array("\\", "/", "\"", "\n", "\r", "\t", "\x08", "\x0c");
  $replacements = array("\\\\", "\\/", "\\\"", "<br>", "\\r", "\\t", "\\f", "\\b");
  $result       = str_replace($escapers, $replacements, $value);
  return $result;
}

/* Create custom endpoints */

function return_api_banner_data($data) {
  $referrer = stripcslashes($_SERVER['HTTP_REFERER']);
  $referrer = str_replace('https://www.westcoasthifi.com.au/', '', $referrer);
  $referrer = str_replace('/', '', $referrer);
  $page = get_page_by_path($referrer);
  $baseUrl       = site_url();
  $bannerContent = str_replace("\n", "", get_field('content', $page->ID));
  $bannerData    = array(
    'bannerUrl' => get_field('banner_image', $page->ID),
    'content'   => get_field('content', $page->ID),
    'baseUrl'   => $baseUrl,
  );

  if (empty($bannerData)) {
    return new WP_Error('no_banner_data', 'No banner data returned', array('status' => 404));
  }

  return $bannerData;
}

function return_featured_accessories_data($data) {

  $baseUrl = site_url();

  $args = array(
    'post_type'  => 'accessories',
    'meta_key'   => 'featured',
    'meta_value' => true,
  );

  $the_query = new WP_Query($args);

  if ($the_query->have_posts()):
    while ($the_query->have_posts()): $the_query->the_post();
      $postId = get_the_ID();

      $accData[] = array(
        'acc_title' => get_the_title(),
        'acc_text'  => get_field('teaser_content', $postId),
        'acc_link'  => get_the_permalink(),
        'acc_bg'    => get_field('background_image', $postId),
      );

    endwhile;
  endif;

  if (empty($accData)) {
    return new WP_Error('no_acc_data', 'No Accessories data returned', array('status' => 404));
  }

  return $accData;
}

add_action('rest_api_init', function () {

  register_rest_route('htprequest', '/banner', array(
    'methods'  => 'GET',
    'callback' => 'return_api_banner_data',
  ));

  register_rest_route('htprequest', '/featured_accessory', array(
    'methods'  => 'GET',
    'callback' => 'return_featured_accessories_data',
  ));

});

// define the woocommerce_get_catalog_ordering_args callback
function filter_woocommerce_get_catalog_ordering_args($args) {
  $args['posts_per_page'] = "-1";
  $args['meta_key']       = "_regular_price";
  //var_dump($args);
  return $args;
};

// add the filter
add_filter('woocommerce_get_catalog_ordering_args', 'filter_woocommerce_get_catalog_ordering_args', 10, 1);

add_filter('gform_pre_render_2', 'populate_posts');
add_filter('gform_pre_validation_2', 'populate_posts');
add_filter('gform_pre_submission_filter_2', 'populate_posts');
add_filter('gform_admin_pre_render_2', 'populate_posts');
function populate_posts($form) {

  foreach ($form['fields'] as &$field) {

    if ($field->type != 'select' || strpos($field->cssClass, 'populate-stores') === false) {
      continue;
    }

    $posts = get_posts('numberposts=-1&post_status=publish&post_type=location');

    $choices = array();
    shuffle($posts);

    foreach ($posts as $post) {
      $choices[] = array('text' => $post->post_title, 'value' => $post->post_title);
    }

    $field->placeholder = 'Please Select a Store';
    $field->choices     = $choices;

  }

  return $form;
}
